/*
 Ejemplo Basico de ESP8266 MQTT

 Este sketch demuestra las capacidades de la libreria pubsub en combinacion con 
 la board ESP8266.

 Se debe conectar a un MQTT y luego:
  - Publica "Hola mundo" Al topic "varios" cada 2 segundos
  - Se suscribe al topic "otros", donde se imprimen varios mensajes
    que reciba. Nota: Se asume que los payloads recibidos son string y no binarios
  - Si el primer caracter del topic "otros" es un 1, se enciende el led del ESP,
    de lo contrario se apaga
*/
#include "DHT.h"
#define DHTPIN 2     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "768232122252";
const char* password = "194327562312";
const char* mqtt_server = "test.denkitronik.com";

WiFiClient clienteWifi;
PubSubClient client(clienteWifi);
DHT dht(DHTPIN, DHTTYPE);
long ultimoMensaje = 0;
char json[180]; 

void setup() {
  pinMode(2, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(2, 0);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  dht.begin();
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Conectandose a ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi conectada");
  Serial.println("Direccion IP: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Llego mensaje [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // encender el LED si se recibio un 1 como primer caracter
  if ((char)payload[0] == '1') {
    Serial.print("Entro en 1");
    digitalWrite(2, 0);   // Enciende el LED (Note que LOW es el nivel del voltaje
    // pero realmente el LED esta encendido; esto es porque es activo en bajo.
    Serial.print("ejecuto digtal Write");
  } else {
    digitalWrite(2, 1);  // Apaga el LED colocando un HIGH
  }

}

void reconnect() {
  // Bucle hasta que reconecte
  while (!client.connected()) {
    Serial.print("Intentando una conexion MQTT...");
    // Intenta conectar
    if (client.connect("clienteWifi")) {
      Serial.println("conectado");
      // Una vez conectado, publica un anuncio...
      client.publish("varios", "Hola mundo");
      // ... y se resuscribe
      client.subscribe("otros");
    } else {
      Serial.print("fallido, rc=");
      Serial.print(client.state());
      Serial.println(" intente de nuevo en 5 segundos");
      // Espere 5 segundos antes de reintentar
      delay(5000);
    }
  }
}

String dhtJson(float h, float t, float f, float hif,  float hic){
  return  "{\"humedad\":\""+String(h)+"\", \"temperaturaC\":\""+ String(t) +"\", \"temperaturaF\":\""+ String(f) +"\", \"indiceCalorF\":\""+ String(hif)+"\", \"indiceCalorC\":\""+ String(hic)+"\"}";
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long ahora = millis();
  if (ahora - ultimoMensaje > 2000) {
    ultimoMensaje = ahora;
    float h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float t = dht.readTemperature();
    // Read temperature as Fahrenheit (isFahrenheit = true)
    float f = dht.readTemperature(true);
    // Compute heat index in Fahrenheit (the default)
    float hif = dht.computeHeatIndex(f, h);
    // Compute heat index in Celsius (isFahreheit = false)
    float hic = dht.computeHeatIndex(t, h, false);
    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t) || isnan(f)) {
      Serial.println(F("Failed to read from DHT sensor!"));
      return;
    }
    //convierte los datos leidos a una cadena en formato json
    dhtJson(h, t, f, hif, hic).toCharArray(json, 180);
    client.publish("dht22", json);
  }
}
